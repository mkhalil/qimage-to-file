#include <iostream>

#include <QtGui/QImage>
#include <QFile>
#include <QTextStream>
#include <QByteArray>

int main()
{
    std::cout << "Reading the image" << std::endl;

    QImage img("untitled.png");
    if(img.isNull()) {
        std::cout << "Unable to read the image" << std::endl;
    } else {
        std::cout << "Format: " << img.format() << std::endl;
        QString filename = "raw_data.txt";
        QFile file(filename);

        std::cout << "Preparing to write to file" << std::endl;
        if(file.open(QIODevice::ReadWrite)) {
            QTextStream out(&file);
            int byteCount = static_cast<int>(img.sizeInBytes());
            std::cout << "Byte Count: " << byteCount << std::endl;
//             QByteArray data = QByteArray(reinterpret_cast<char *>(img.bits()), byteCount);
//             for(int i = 0; i < byteCount - 1; i++) {
//                 char *c = data.data() + i;
//                 out << c;
//             }

            unsigned char *dataPtr = img.bits();
            for(int i = 0; i < byteCount; i++) {
                char *c = (char *)(dataPtr + i);
                out << c;
            }

            out.flush();
            std::cout << "Flushed and closed file after writing" << std::endl;
        }

        file.close();
    }

    return 0;
}
